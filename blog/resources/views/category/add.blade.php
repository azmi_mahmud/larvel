@extends('../layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-1">
            
                <h1 class="page-header">New category's</h1>
                @if (count($errors) > 0)
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif

                {!! Form::open(array('url' => '/category/add')) !!}
				     
				    {{Form::label('email', 'Category name :')}} 
				    {{Form::text('text', '',array('class' => 'form-control' , 'name' => 'name' , 'placeholder' => 'Please Enter Article Title'))}}
				   
                                   
                                    {{Form::label('email', 'Category  description :')}} 
				    {{Form::textarea('text', '',array('class' => 'form-control' , 'name' => 'description'  , 'placeholder' => 'Please Enter Article Title'))}}
				    {{Form::hidden('text', Auth::user()->id ,array('class' => 'form-control' ,  'name' => 'user_id'  ))}}
                                    
                                    <br>
					{{Form::submit('Confirm!' , array('class'=>'btn btn-info' , 'name' => 'submit' ))}}
				{!! Form::close() !!}	
            
        </div>
    </div>
</div>
@endsection
