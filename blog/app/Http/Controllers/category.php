<?php

namespace App\Http\Controllers;



/*use Illuminate\Http\Request;

use App\Http\Requests;*/


use DB;
use Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\article;
use App\comment;

class category extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function  add()
    {
        
       
       
        return view('category.add');  
  

    }
    
    
public function insert(Request $request){
    
    
    $this->validate($request, [
	        'name' => 'required|unique:category|max:255',
	        'description' => 'required',
	        
	    ]);
	    DB::table('category')->insert(
		    ['name' => $request['name'], 'description' => $request['description'] 
                       ]
		);

		return Redirect::to('/home')->with('message', 'New category Created');

    
}
    
    
}