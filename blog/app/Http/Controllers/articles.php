<?php

namespace App\Http\Controllers;



/*use Illuminate\Http\Request;

use App\Http\Requests;*/


use DB;
use Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\article;
use App\comment;

class articles extends Controller
{
    public $categorys;
    public function __construct()
    {
        $this->categorys = DB::table('category')->get();
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getcategory($id){
       
         $articles= DB::table('articles')
            ->where([['category_id',$id],['active','1']])->get();
         
             return view('home')->with('articles', $articles)->with('categores', $this->categorys);
         //dd($articles);
        
    }
    
    
    public function index()
    {
       
        $articles= DB::table('articles')
            ->join('users', 'articles.user_id', '=', 'users.id')->get();
        //$articles=article::all();


        if(count($articles) > 0){
            //var_dump($articles);
            return view('home' , ['articles' => $articles]);    
        }
        else{
            echo "No Data Found";

        
        
        //return view('home' , , ['tasks' => $tasks]);
        }
    }

    public function show($id)
    {

        $article= DB::table('articles')
            ->where('id', $id)->get();

        $comments= DB::table('comments')
            ->where('article_id', $id)->get();    
        //var_dump($article);


        if(count($article) > 0){
            //var_dump($articles);
            return view('articles/show' , ['article' => $article , 'categores'=>$this->categorys,'comments' => $comments]);    
        }
        else{
            echo "No Data Found";
        }
    }

    public function create()
    {
        $this->middleware('auth');
    	$categorys= DB::table('category')->select('name','id')->get();
            foreach ($categorys as $category ){
                
                $viewCategoryArray[$category->id] = $category->name ;
            }
        
        return view('articles.create',['category' => $viewCategoryArray ]);    
        
    }

    public function update_form($id)
    {
        //echo "aaaaaaaaaa";
        $article= DB::table('articles')
            ->where('id', $id)->get();
            if(count($article) > 0){
                return view('articles.update' , ['article' => $article]);         
                
                //return Redirect::to('/home');
            } 
            else{
                return Redirect::to('/home');
            } 
        
            
        
    }


    public function update(Request $request)
    {
        
        $article= DB::table('articles')
            ->where('id', $_POST['id'])->get();
            if(count($article) > 0){
                if($article[0]->user_id == Auth::user()->id){
                    $this->validate($request, [
                        'title' => 'required|max:255',
                        'articles' => 'required',
                        'user_id' => 'required|integer',
                        'id' => 'required|integer',
                    ]);

                    DB::table('articles')
                        ->where('id', $_POST['id'])
                        ->update(['title' => $_POST['title'] , 'articles' => $_POST['articles'] , ] );
                    return Redirect::to('/articles/edit/'.$_POST['id']);    
                }
                else{
                    return Redirect::to('/home');
                }
            } 
            else{
                return Redirect::to('/home');
            } 
        
            
        
    }



    public function insert(Request $request)
    {
       
    	$this->validate($request, [
	        'title' => 'required|unique:articles|max:255',
	        'articles' => 'required',
	        'user_id' => 'required|integer',
	    ]);
	    DB::table('articles')->insert(
		    ['title' => $request['title'], 'articles' => $request['articles'] ,
                        'category_id'=>$request['category_id'], 'user_id' => $request['user_id'],
                        'active'=>$request['active']]
		);

		return Redirect::to('/home')->with('message', 'New Article Created');

    	  
        
    }


    public function delete($id)
    {
        //echo "aaaaaaaaaa";
        
                    
                    DB::table('articles')->where([['id', '=', $id ],['user_id','=',]] )->delete();
                    DB::table('comments')->where('id', '=', $id)->delete();
                    return Redirect::to('/home');

           
           
        
    }


}
