<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('articles/getcategory/{id}', 'articles@getcategory');


/************** Auth  ************************/
// Login Routes 
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');



Route::get('/profile', array('before' => 'auth', function()
{
    
    // Only authenticated users may enter...
}));



Route::auth();

Route::get('/home', 'HomeController@index');
Route::get('/articles/', 'articles@index');
Route::get('/articles/new', 'articles@create');
Route::post('/articles/create', 'articles@insert');
Route::post('/category/add', 'category@insert');


Route::get('/articles/{id}', 'articles@show');
Route::get('/articles/delete/{id}', 'articles@delete');
Route::get('/articles/new', 'articles@create');
Route::get('/category/add', 'category@add');
Route::get('/articles/edit/{id}', 'articles@update_form');
Route::post('/articles/update', 'articles@update');


Route::get('/comments/{id}', 'commentsController@delete');
Route::post('/comments/create', 'commentsController@insert');

